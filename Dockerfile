FROM node:alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json .
RUN npm install

COPY . .

ENV CONTENT_API_URL http://localhost:3001

EXPOSE 3000
CMD ["npm", "start"]
